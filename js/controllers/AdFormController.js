import DataService from "../services/DataService.js";
import PubSub from "../services/PubSub.js";

export default class AdFormController {
  constructor(element) {
    this.element = element;
    this.attachedEventListeners();
  }

  attachedEventListeners() {
    this.element.addEventListener("submit", async (event) => {
      event.preventDefault();

      if (this.element.checkValidity()) {
        const data = new FormData(this.element);
        const name = data.get("name");
        const price = data.get("price");
        const photo = data.get("photo");
        const sell = data.get("sell");
        const tag = data.get("tag");

        //const adProperties = {name, price, photo, sell, tag}

        try {
          const result = await DataService.createAd(
            name,
            price,
            photo,
            sell,
            tag
          );
          PubSub.publish(PubSub.events.SHOW_SUCCESS, "Created ad");
          if (result) {
            window.location.href = "/indexMain.html";
          }
        } catch (error) {
          PubSub.publish(PubSub.events.SHOW_ERROR, error);
        }
      }
    });
  }
}
