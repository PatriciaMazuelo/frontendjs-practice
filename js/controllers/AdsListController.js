import { adView } from '../views.js'
import DataService from '../services/DataService.js'

export default class AdsListController{
    constructor(element, errorMessageController){
        this.element = element
        this.errorMessageController = errorMessageController
    }

    async renderAds(){
        try{
            const ads = await DataService.getAds();
            for(const ad of ads) {
                const adElement = document.createElement("div");
                adElement.innerHTML = adView(ad) 
                this.element.appendChild(adElement)
            }
            
        }catch(error){
            this.errorMessageController.showError(error)
        }
    }
}