import ErrorMessageController from "./controllers/ErrorMessageController.js"
import AdsListController from "./controllers/AdsListController.js"

window.addEventListener("DOMContentLoaded", function(){
    
    const errorDiv = document.querySelector(".error-message");
    const errorMessageController = new ErrorMessageController(errorDiv);

    const adListDiv = document.querySelector(".adListContainer");
    
    const adListController = new AdsListController(adListDiv, errorMessageController);

    adListController.renderAds();


})