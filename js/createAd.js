import AdFormController from "./controllers/AdFormController.js";
import MessageController from "./controllers/MessageController.js";
import DataService from "./services/DataService.js";


window.addEventListener("DOMContentLoaded", function(){
    
    if (DataService.isAuthenticed() === false) {
         window.location.href = "/indexLogin.html"
    }
    

    const form = document.querySelector("form");

    new AdFormController(form);

    const ads = document.querySelector(".ads");

    new MessageController(ads)
})