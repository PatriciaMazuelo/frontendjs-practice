import SignUpController from "./controllers/SignUpController.js";
import MessageController from "./controllers/MessageController.js"

window.addEventListener("DOMContentLoaded", function(){
    //seleccionamos el nodo del formulario
    const form = document.getElementById("form");
    
    new SignUpController(form);
    
     
    const messages = document.querySelector('.error-message')

    
    new MessageController(messages)
})