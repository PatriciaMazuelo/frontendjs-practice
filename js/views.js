export function adView(ad){
    return `
        <div class = "ad">
            <img class = "photo"src="${ad.photo}" width="250" height="200">
            <a href="/indexDetail.html?id=${ad.id}"><strong class = "name">${ad.name}</strong></a>
            <p class = "price">${ad.price}</p>
            <p class = "sell">${ad.sell}</p>
            <p class="author">${ad.author}</p>
        </div>`
}


export function errorView(message) {
    return `<div class="error">
        ${message}
        <button>Cerrar</button>
    </div>`
}

export function successView(message) {
    return `<div class="success">
        ${message}
        <button>Cerrar</button>
    </div>`
}

export function adDetailView(ad){

    if(ad===null){

        return "<h1>Nada que mostrar</h1>"
    }

    let button = " "
    if (ad.canBeDeleted) {
        button = '<button class="delete">Borrar</button>'
        
    }
    
    return `
    <img class = "photo"src="${ad.photo}" width="250" height="200"></img><br>
    <strong class = "name">${ad.name}</strong>
    <p class = "price">Price: ${ad.price}</p>
    <p class = "sell">Buy/Sell: ${ad.sell}</p>
    <p class="author">Created by: ${ad.author}</p>
    ${button}
    `
    
}

export function loaderView() {
    return '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>'
}