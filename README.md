# FrontendJS practice

## NodePOP

Tú página de anuncios. Compra y vende los mejores productos con nosotros.

### Composición de la página 
* Registro
* Login
* Principal
* Crear anuncio
* Detalle


### Pasos a seguir
Está página web usa la base de datos [Sparrest](https://github.com/kasappeal/sparrest.js).

1. Accede a la base de datos y copia el archivo ads.json. 
1. Resgístrate en la página de registro.
1. Haz login con tu usuario y contraseña. Esto te redirigira a la página principal.
1. Crea tu anuncio.
1. Tienes la opción de borrarlo o de crear más.

